import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../valid/mixin_valid.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login me',
      home: Scaffold(
        body: LoginScreen(),
      ),
    );
  }
}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<StatefulWidget> with Validation {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(20.0),
        child: Form(
            key: formKey,
            child: Column(
              children: [
                emailField(),
                passwordField(),
                buttonField(),
              ],
            )));
  }

  Widget emailField() {
    return TextFormField(
        decoration: InputDecoration(labelText: "Email address"),
        validator: ValidationEmail,
        onSaved: (value) {
          print('OnSaved: value=$value');
        });
  }

  Widget passwordField() {
    return TextFormField(
        decoration: InputDecoration(labelText: "Password"),
        validator: ValidationPassword,
        onSaved: (value) {
          print('onSaved: value=$value');
        });
  }

  Widget buttonField() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();
          }
        },
        child: Text('Submit'));
  }
}
