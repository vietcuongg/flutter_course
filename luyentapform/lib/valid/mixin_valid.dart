mixin Validation {
  String? ValidationEmail(String? value) {
    if (value!.isEmpty) {
      return "Email is required";
    } else if (!value.contains('@')) {
      return "Your email missing'@'";
    } else if (!value.contains('.')) {
      return "Your email Must have '.'";
    } else if (!value.contains('com')) {
      return "Your email must have 'com'";
    }
  }

  String? ValidationPassword(String? value) {
    if (value!.isEmpty) {
      return "Password is required.";
    } else if (!value.contains(RegExp(r'[A-Z]'))) {
      return "Password needs at least 1 capital letter.";
    } else if (!value.contains(RegExp(r'[a-z]'))) {
      return "Password needs at least 1 lowercase letter.";
    } else if (!value.contains(RegExp(r'[!@#$%^&*(),.?":{}|<>]'))) {
      return "Password needs at least 1 special letter.";
    } else if (value.length < 8) {
      return "Password must have at least 8 characters.";
    } else {
      return null;
    }
  }
}
