import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:theory5/bloc/bloc.dart';
import 'package:theory5/valid/mixin_valid.dart';
import '../bloc/bloc.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login me',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Login'),
        ),
        body: LoginScreen(),
      ),
    );
  }
}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<StatefulWidget> with Validation {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;

  final bloc = Bloc();
  String errorMessage = '';
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(20.0),
        child: Form(
            key: formKey,
            child: Column(
              children: [
                emailField(),
                passwordField(),
                buttonField(),
              ],
            )));
  }

  Widget emailField() {
    return TextFormField(
      decoration: InputDecoration(
        icon: Icon(Icons.mail),
        labelText: "Email address",
        errorText: errorMessage,
      ),
      validator: ValidationEmail,
      onSaved: (value) {
        email = value as String;
      },
      onChanged: (String value) {
        print('onChanged email:$value');
        //bloc.getEmailStreamController().sink.add(value);
        //bloc.emailStreamController().sink.add(value);
        //bloc.changeEmail(value);
        bloc.changeEmail(value);
        // bloc.emailStream.Lise
        if (!value.contains('@')) {
          setState(() {
            errorMessage = '$value is an invalid mail';
          });
        } else {
          errorMessage = '';
        }
        setState(() {
          print('Update the screen state');
        });
      },
    );
  }

  Widget passwordField() {
    return TextFormField(
      decoration:
          InputDecoration(icon: Icon(Icons.password), labelText: "Password"),
      validator: ValidationPassword,
      onSaved: (value) {
        password = value as String;
      },
      onChanged: (String value) {
        print('onChanged password:$value');
      },
    );
  }

  Widget buttonField() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();
          }
        },
        child: Text('Submit'));
  }
}
