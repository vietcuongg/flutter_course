import 'package:flutter/material.dart';
import 'package:lab4/lastpage/dientu.dart';
import 'package:lab4/lastpage/freshfood.dart';
import 'package:lab4/lastpage/maytinh.dart';
import 'package:lab4/lastpage/toysMeBe.dart';
import 'package:lab4/lastpage/dienthoai.dart';
import '../ui/CategoryPage.dart';

class suggestionPage extends StatefulWidget {
  static const route = '/suggestion';

  @override
  State<StatefulWidget> createState() {
    return suggestionPageState();
  }
}

class suggestionPageState extends State<StatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(204, 51, 48, 48),
        title: Text("Danh mục đang hot:"),
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: GridView(
          primary: false,
          children: [
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Color.fromARGB(255, 221, 136, 130)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.child_care,
                      size: 40,
                    ),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => toysMeBe()));
                    },
                  ),
                  Text(
                    "Đồ chơi - Mẹ & Bé",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Color.fromARGB(255, 7, 7, 7), fontSize: 15),
                  ),
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Color.fromARGB(255, 231, 219, 105)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.food_bank,
                      size: 44,
                    ),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => freshfood()));
                    },
                  ),
                  Text(
                    "Ngon",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color.fromARGB(255, 7, 7, 7),
                      fontSize: 15,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Color.fromARGB(255, 80, 238, 233)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => dienthoai()));
                    },
                    icon: Icon(
                      Icons.phone_android,
                      size: 43,
                    ),
                  ),
                  Text(
                    "Điện Thoại-Máy Tính",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Color.fromARGB(255, 7, 7, 7)),
                  ),
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Color.fromARGB(255, 127, 237, 141)),
              child: Column(
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.laptop,
                      size: 45,
                    ),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => maytinh()));
                    },
                  ),
                  Text(
                    "Laptop",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Color.fromARGB(255, 7, 7, 7)),
                  ),
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Color.fromARGB(255, 160, 83, 218)),
              child: Column(
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.headphones,
                      size: 48,
                    ),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => dientu()));
                    },
                  ),
                  Text(
                    "Thiết bị điện tử",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Color.fromARGB(255, 7, 7, 7)),
                  ),
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Color.fromARGB(255, 219, 69, 119)),
              child: Column(
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.book,
                      size: 45,
                    ),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => dientu()));
                    },
                  ),
                  Text(
                    "Sách hay",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Color.fromARGB(255, 7, 7, 7)),
                  ),
                ],
              ),
            ),
          ],
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            mainAxisSpacing: 20,
            crossAxisSpacing: 10,
          ),
        ),
      ),
    );
  }
}
