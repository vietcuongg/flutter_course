import 'package:flutter/material.dart';
import '../ui/CategoryPage.dart';

class foodPage extends StatefulWidget {
  static const route = "/freshfood";
  @override
  State<StatefulWidget> createState() {
    return foodPageState();
  }
}

class CardItem {
  final String urlImage;

  const CardItem({
    required this.urlImage,
  });
}

class foodPageState extends State<StatefulWidget> {
  List<CardItem> items = [
    CardItem(
        urlImage:
            'https://bloganchoi.com/wp-content/uploads/2017/02/khuyen-mai-hot-giam-gia-soc-tiki.jpg'),
    CardItem(
        urlImage:
            'https://salt.tikicdn.com/cache/w1080/ts/banner/92/9c/17/0d2ca5db062b7f17d01189ef71be74e8.png.webp'),
    CardItem(
        urlImage:
            'https://salt.tikicdn.com/cache/w1080/ts/banner/ca/47/a3/4a16339948efc388e6d00ba3c64856cc.jpg.webp'),
    CardItem(
        urlImage:
            'https://salt.tikicdn.com/ts/brickv2og/e5/b6/6f/bc56631bfc511b03524ea75b27d91ba5.png'),
    CardItem(
        urlImage:
            'https://static.accesstrade.vn/publisher/www/files/img_promo/offer/img/tikivn/Tiki_0103.jpg'),
    CardItem(
        urlImage:
            'https://i.pinimg.com/originals/e7/ad/3b/e7ad3b4e839e12a589a6e079833b087f.jpg'),
    CardItem(
        urlImage:
            'https://bloganchoi.com/wp-content/uploads/2017/02/khuyen-mai-hot-giam-gia-soc-tiki.jpg'),
    CardItem(
        urlImage:
            'https://www.tapchicongthuong.vn/images/Uploaded/Share/2017/10/12/1faKM-thit-tuoi-songresize.jpg'),
    CardItem(
        urlImage:
            'https://tabimkhuyenmai.vn/wp-content/uploads/2019/10/unidry-khuyen-mai-2048x1182.png'),
    CardItem(
        urlImage:
            'https://omron-yte.com.vn/wp-content/uploads/2020/09/km-tiki-29-9-30-9-pc.jpg'),
    CardItem(
        urlImage:
            'https://lh5.googleusercontent.com/proxy/XbuRgTqlJH0zZ5MOx0bbT9PH3veWSfYi01JgFBAFkdCZS4uEFCDfhbw=s0-d'),
    CardItem(
        urlImage:
            'https://salt.tikicdn.com/ts/brickv2og/e5/b6/6f/bc56631bfc511b03524ea75b27d91ba5.png'),
    CardItem(
        urlImage:
            'https://www.daygiare.com/public/storage/photo/2019/12/14/tiki-khuyen-mai-banh-kinh-do-giam-den-30-0.png'),
    CardItem(
        urlImage:
            'https://daygiare.com/public/storage/photo/2019/12/15/lazada-mung-giang-sinh-giam-den-70-thuc-pham-kho-do-hop-0.jpeg'),
  ];
  final List<String> toysList = <String>[
    'Trái Cây',
    'Thịt Trứng',
    'Cá, thủy hải sản   ',
    'Rau củ quả',
    'Sữa, bơ, phô mai',
  ];
  final List<List<String>> imagesList = <List<String>>[
    [
      'https://salt.tikicdn.com/cache/200x200/ts/product/b3/cc/a9/0cf4656abb03be4ae059e04408c5b985.png.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/d7/aa/cb/e9a281c9920606bc7fab36e1150b93d6.png.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/9a/0a/c2/e812b1ea4ade7463d3600be42c204cb8.jpg.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/36/83/7e/487e2def6c5534850ec09bae6337f08e.jpg.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/96/f0/4d/2aec933e84f4fd4b80339fa65ba25e3b.jpg.webp',
    ],
    [
      'https://salt.tikicdn.com/cache/200x200/ts/product/db/fc/da/099f44cd6b5cc3f2ebf0ec163c1cda2c.jpg.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/2b/9e/76/b6e0bafa30c15bf31302920dbca6b579.jpg.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/99/e5/7f/cce04501a22d2c00fe358df7134285f4.jpg.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/67/e2/b1/7982e591f7394a63499625c9e353e64d.png.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/75/c3/71/b302a864c69fdf0bab9c58914bb6d090.jpg.webp',
    ],
    [
      'https://salt.tikicdn.com/cache/200x200/ts/product/d1/08/d0/fb88d94a8b8125d1d4f9dfcba40d9b74.png.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/9b/fd/77/b19cb1d22d09406f686b99244908491c.jpg.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/db/94/96/3afaa92acd28372700ae9608db1bd419.jpg.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/29/76/f8/0873186fe2383f0489247cf9855bfed5.jpg.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/9a/60/ce/d4cbaa7a2723b77c7c1daa1cd8d8022a.png.webp',
    ],
    [
      'https://salt.tikicdn.com/cache/200x200/ts/product/5b/7b/c0/390674389e49bee13cce385370c13385.png.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/3d/d8/31/b532b3c84369c60cb5c2bfb676f2fe4c.JPG.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/6b/42/fc/fd74237b1d3053434e412e32b27a7023.jpg.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/55/9d/8e/c4dbc73646daa85fa1a3d41a3341fcfe.jpg.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/b7/06/e9/47f1bdb14b810ff1fbf8e21335518dd8.jpg.webp',
    ],
    [
      'https://salt.tikicdn.com/cache/200x200/ts/product/37/94/82/2043a13444c306e9ff57d6bfb9bd9b9b.jpg.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/99/7e/e2/cd5e9ae88c22fd8897a14cd05cfed146.jpg.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/22/d4/c6/46e56310e7cf32939534606480c138c3.jpg.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/87/de/39/17ae4ccfca061174e4dcc4f3ae46f1d5.jpg.webp',
      'https://salt.tikicdn.com/cache/200x200/ts/product/9f/f7/cf/4597b86760c11dac2297bf8d4cf2e690.png.webp',
    ],
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Đồ chơi- Mẹ & Bé',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
          backgroundColor: Color.fromARGB(255, 228, 210, 210),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.keyboard_arrow_right,
                color: Colors.black,
              ),
              onPressed: () {},
            )
          ],
        ),
        body: Container(
          height: 600,
          child: Column(children: [
            Container(
              height: 150,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) => buildCard(item: items[index]),
                itemCount: items.length,
              ),
            ),
            Container(
                height: 382,
                padding: EdgeInsets.all(10),
                color: Color.fromARGB(255, 228, 227, 227),
                child: ListView.separated(
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                          height: 120,
                          color: Colors.white,
                          padding: EdgeInsets.all(5),
                          child: Column(children: [
                            Row(children: [
                              Text(
                                '${toysList[index]}',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Spacer(),
                              TextButton(
                                  onPressed: () {
                                    //Navigator.push(context, MaterialPageRoute(builder: (context) => clothes_screens[index]));
                                  },
                                  child: Text('TẤT CẢ'))
                            ]),
                            Container(
                                height: 60,
                                child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: 4,
                                    itemBuilder:
                                        (BuildContext context, int imgindex) {
                                      return Container(
                                          width: 70,
                                          decoration: new BoxDecoration(
                                              image: new DecorationImage(
                                                  fit: BoxFit.fill,
                                                  image: new NetworkImage(
                                                      '${imagesList[index][imgindex]}'))));
                                    }))
                          ]));
                    },
                    separatorBuilder: (BuildContext context, int index) =>
                        const Divider(),
                    itemCount: 5))
          ]),
        ));
  }

  Widget buildCard({
    required CardItem item,
  }) =>
      Container(
        child: Column(children: [
          Expanded(
              child: Image.network(
            item.urlImage,
            fit: BoxFit.cover,
          )),
        ]),
      );
}
