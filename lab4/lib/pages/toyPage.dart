import 'package:flutter/material.dart';
import '../ui/CategoryPage.dart';

class toyPage extends StatefulWidget {
  static const Route = '/toyPage';

  @override
  State<StatefulWidget> createState() {
    return toyPageState();
  }
}

class CardItem {
  final String urlImage;

  const CardItem({
    required this.urlImage,
  });
}

class toyPageState extends State<StatefulWidget> {
  List<CardItem> items = [
    CardItem(
        urlImage:
            'https://cuocsongdigital.com/wp-content/uploads/2018/09/hinh3-khuyen-mai-tiki-zalopay.jpg'),
    CardItem(
        urlImage:
            'https://i.pinimg.com/originals/52/58/cd/5258cdb8e9b73d704d6a45c1ed383b07.jpg'),
    CardItem(
        urlImage:
            'https://th.bing.com/th/id/R.e3a5579ebd921a464d7f279592db211d?rik=5oleNSBiySO%2b8w&riu=http%3a%2f%2fvnc52.com%2fwp-content%2fuploads%2f2021%2f03%2fsu-kien-tiki-11-tuoi.png&ehk=10uZsxClqiAt6cRb4%2bmCKh4TWw0QUgwEm%2bCHIQ2Uru4%3d&risl=&pid=ImgRaw&r=0'),
    CardItem(
        urlImage:
            'https://salt.tikicdn.com/ts/brickv2og/e5/b6/6f/bc56631bfc511b03524ea75b27d91ba5.png'),
    CardItem(
        urlImage:
            'https://static.accesstrade.vn/publisher/www/files/img_promo/offer/img/tikivn/Tiki_0103.jpg'),
    CardItem(
        urlImage:
            'https://i.pinimg.com/originals/e7/ad/3b/e7ad3b4e839e12a589a6e079833b087f.jpg'),
    CardItem(
        urlImage:
            'https://bloganchoi.com/wp-content/uploads/2017/02/khuyen-mai-hot-giam-gia-soc-tiki.jpg'),
  ];
  final List<String> toysList = <String>[
    'Tã, Bỉm',
    'Đồ Chơi',
    'Đồ dùng cho bé',
    'Dinh Dưỡng',
    'Thời Trang',
  ];
  final List<List<String>> imagesList = <List<String>>[
    [
      'https://tse1.mm.bing.net/th/id/OIP.RmU3BeeTfuBDOprERKkTgAHaHa?w=192&h=192&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse3.mm.bing.net/th/id/OIP.bW5aPKH_VNTyMOiXcA8jRwHaHa?w=192&h=192&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse1.mm.bing.net/th/id/OIP.RmU3BeeTfuBDOprERKkTgAHaHa?w=192&h=192&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse3.mm.bing.net/th/id/OIP.bW5aPKH_VNTyMOiXcA8jRwHaHa?w=192&h=192&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse1.mm.bing.net/th/id/OIP.yCJQHy9KXg36wgX45fAvagHaHa?w=216&h=193&c=7&r=0&o=5&dpr=1.25&pid=1.7',
    ],
    [
      'https://tse4.mm.bing.net/th/id/OIP.xxwdXvQLkj5iXvbnoL1SRAHaHa?w=148&h=180&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse1.mm.bing.net/th/id/OIP.qq5US8CiFVdloBEOA7F_CAHaE1?w=266&h=180&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse3.mm.bing.net/th/id/OIP.YpxuwPk_NP40JziuMCTemgHaEL?w=289&h=186&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse2.mm.bing.net/th/id/OIF.LZuxzMMNER0JutvHip442A?w=214&h=214&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse1.mm.bing.net/th/id/OIP.yCJQHy9KXg36wgX45fAvagHaHa?w=216&h=193&c=7&r=0&o=5&dpr=1.25&pid=1.7',
    ],
    [
      'https://tse3.mm.bing.net/th/id/OIP.uX91qy6RtWKkzzm4jgztOQHaHa?w=197&h=197&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse1.mm.bing.net/th/id/OIP.sZHvXi1tDg3DIQ-VN5KyTQHaHa?w=166&h=180&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse2.mm.bing.net/th?id=OIF.G5dmzA%2fOiXyepEEYiYtD6g&w=282&h=188&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse4.mm.bing.net/th?id=OIF.MFHbbwE2v6Q%2beZ%2f%2bPLoqZA&w=173&h=184&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse1.mm.bing.net/th/id/OIP.yCJQHy9KXg36wgX45fAvagHaHa?w=216&h=193&c=7&r=0&o=5&dpr=1.25&pid=1.7',
    ],
    [
      'https://tse2.mm.bing.net/th/id/OIP.KST2y2POpX86jYACOr7VlwHaKO?w=140&h=194&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse1.mm.bing.net/th/id/OIP.DkXmCDCiiOJgSXJwwcC57QHaHa?w=187&h=187&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse2.mm.bing.net/th/id/OIF.1XYjXPN9SL5XzgEchBHlPw?w=181&h=181&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse1.mm.bing.net/th/id/OIP.So-06Bb7KNh_XMeCcNDY4gHaHa?w=196&h=194&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse1.mm.bing.net/th/id/OIP.yCJQHy9KXg36wgX45fAvagHaHa?w=216&h=193&c=7&r=0&o=5&dpr=1.25&pid=1.7',
    ],
    [
      'https://tse2.mm.bing.net/th/id/OIP.PQYs8UbmCQ1IXn4tJI6KmQHaHc?w=199&h=200&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse3.mm.bing.net/th/id/OIP.JrKIq6tWT5afFQu3VDbucgHaHa?w=173&h=180&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse3.mm.bing.net/th/id/OIF.6Pqsmy7yxtALq9vVQ0fkLg?w=253&h=182&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse3.mm.bing.net/th/id/OIP.EyuMQkN8GOzxCW01pjtDVgHaHa?w=182&h=182&c=7&r=0&o=5&dpr=1.25&pid=1.7',
      'https://tse1.mm.bing.net/th/id/OIP.yCJQHy9KXg36wgX45fAvagHaHa?w=216&h=193&c=7&r=0&o=5&dpr=1.25&pid=1.7',
    ],
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Đồ chơi- Mẹ & Bé',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
          backgroundColor: Color.fromARGB(255, 228, 210, 210),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.keyboard_arrow_right,
                color: Colors.black,
              ),
              onPressed: () {},
            )
          ],
        ),
        body: Container(
          height: 600,
          child: Column(children: [
            Container(
              height: 150,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) => buildCard(item: items[index]),
                itemCount: items.length,
              ),
            ),
            Container(
                height: 382,
                padding: EdgeInsets.all(10),
                color: Color.fromARGB(255, 228, 227, 227),
                child: ListView.separated(
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                          height: 120,
                          color: Colors.white,
                          padding: EdgeInsets.all(5),
                          child: Column(children: [
                            Row(children: [
                              Text(
                                '${toysList[index]}',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Spacer(),
                              TextButton(
                                  onPressed: () {
                                    //Navigator.push(context, MaterialPageRoute(builder: (context) => clothes_screens[index]));
                                  },
                                  child: Text('TẤT CẢ'))
                            ]),
                            Container(
                                height: 60,
                                child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: 4,
                                    itemBuilder:
                                        (BuildContext context, int imgindex) {
                                      return Container(
                                          width: 70,
                                          decoration: new BoxDecoration(
                                              image: new DecorationImage(
                                                  fit: BoxFit.fill,
                                                  image: new NetworkImage(
                                                      '${imagesList[index][imgindex]}'))));
                                    }))
                          ]));
                    },
                    separatorBuilder: (BuildContext context, int index) =>
                        const Divider(),
                    itemCount: 5))
          ]),
        ));
  }

  Widget buildCard({
    required CardItem item,
  }) =>
      Container(
        child: Column(children: [
          Expanded(
              child: Image.network(
            item.urlImage,
            fit: BoxFit.cover,
          )),
        ]),
      );
}
