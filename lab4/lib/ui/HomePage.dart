import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab4/ui/ChatPage.dart';
import './CategoryPage.dart';
import 'NewPage.dart';
import 'CategoryPage.dart';

class HomeScreen extends StatefulWidget {
  static const route = '/';

  @override
  State<StatefulWidget> createState() => HomeScreenState();
}

class HomeScreenState extends State<StatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildHomeScreen(),
    );
  }

  Widget buildHomeScreen() {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color.fromARGB(255, 214, 122, 230),
              Color.fromARGB(255, 48, 46, 155)
            ]),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "FREESHIP",
                style: TextStyle(
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Colors.white),
              ),
              SizedBox(
                width: 80,
              ),
              Text(
                "TIKII",
                style: TextStyle(
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Colors.white),
              ),
              SizedBox(
                width: 80,
              ),
              IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.notification_add_outlined)),
              IconButton(onPressed: () {}, icon: Icon(Icons.shopping_cart))
            ],
          ),
          SizedBox(height: 5),
          CupertinoSearchTextField(
            backgroundColor: Colors.white,
            placeholder: 'Bạn muốn tìm gì ?',
          ),
          Container(
            height: 100.0,
            child: ListView(
                padding: EdgeInsets.all(5),
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      'Thịt, Rau Củ',
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      'Bách Hóa',
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      'Nhà Cửa',
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      'Điện tử',
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      'Thiết Bị',
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      'Quần Áo',
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                  ),
                ]),
          ),
        ],
      ),
    );
  }
}
