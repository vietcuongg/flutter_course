import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:lab4/main.dart';
import 'HomePage.dart';
import '../pages/suggestionPage.dart';
import '../pages/elecdevicePage.dart';
import '../pages/foodPage.dart';
import '../pages/phonePage.dart';
import '../pages/lapPage.dart';
import '../pages/toyPage.dart';

class CategoryPage extends StatefulWidget {
  @override
  CategoryPageState createState() => CategoryPageState();
}

class CategoryPageState extends State<CategoryPage> {
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: 590,
            child: Row(
              //thanh goi y danh muc cac loai san pham
              children: <Widget>[
                NavigationRail(
                  backgroundColor: Color.fromARGB(204, 74, 72, 72),
                  selectedIndex: index,
                  selectedIconTheme:
                      IconThemeData(color: Colors.white, size: 50),
                  unselectedIconTheme: IconThemeData(
                      color: Color.fromARGB(255, 240, 149, 3), size: 50),
                  onDestinationSelected: (index) {
                    setState(() {
                      this.index = index;
                    });
                  },
                  destinations: [
                    NavigationRailDestination(
                      icon: Icon(Icons.star_border),
                      label: Text('Gợi ý cho bạn'),
                    ),
                    NavigationRailDestination(
                      icon: Icon(Icons.child_care),
                      label: Text('Đồ chơi -Mẹ & bé'),
                    ),
                    NavigationRailDestination(
                      icon: Icon(Icons.food_bank),
                      label: Text('Ngon'),
                    ),
                    NavigationRailDestination(
                      icon: Icon(Icons.phone_android),
                      label: Text('Điện thoại - Máy tính bảng'),
                    ),
                    NavigationRailDestination(
                      icon: Icon(Icons.laptop),
                      label: Text('Laptop'),
                    ),
                    NavigationRailDestination(
                      icon: Icon(Icons.headphones),
                      label: Text('Thiết bị điên tử'),
                    ),
                  ],
                ),
                Expanded(child: buildPages()),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget buildPages() {
    switch (index) {
      case 0:
        return suggestionPage();
      case 1:
        return toyPage();
      case 2:
        return foodPage();
      case 3:
        return phonePage();
      case 4:
        return lapPage();
      case 5:
        return elecdevicePage();
      default:
        return defaultPage();
    }
  }
}

class defaultPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: Text('good?.')),
    );
  }
}
