import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:lab4/main.dart';
import 'CategoryPage.dart';
import 'homePage.dart';
import 'NewPage.dart';
import 'ChatPage.dart';

import 'package:lab4/lastpage/freshfood.dart';
import 'package:lab4/lastpage/toysMeBe.dart';
import 'package:lab4/lastpage/dienthoai.dart';
import 'package:lab4/lastpage/maytinh.dart';
import 'package:lab4/lastpage/dientu.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Routing For App',
      routes: {
        toysMeBe.route: (context) => toysMeBe(),
        freshfood.route: (context) => freshfood(),
        dienthoai.route: (context) => dienthoai(),
        maytinh.route: (context) => maytinh(),
        dientu.route: (context) => dientu(),
      },
      home: AppHomePage(),
    );
  }
}

class AppHomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AppState();
}

class AppState extends State<AppHomePage> {
  int currentIndex = 0;
  final screens = [HomeScreen(), CategoryPage(), SalePage(), ChatPage()];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: IndexedStack(
        index: currentIndex,
        children: screens,
      ),
      bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          iconSize: 35,
          backgroundColor: Colors.white,
          selectedItemColor: Color.fromARGB(255, 71, 130, 179),
          onTap: (index) {
            setState(() {
              currentIndex = index;
            });
          },
          currentIndex: currentIndex,
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.home),
                label: "Home",
                backgroundColor: Colors.blue),
            BottomNavigationBarItem(
                icon: Icon(Icons.category),
                label: "Category",
                backgroundColor: Colors.blue),
            BottomNavigationBarItem(
                icon: Icon(Icons.auto_awesome),
                label: "New",
                backgroundColor: Colors.blue),
            BottomNavigationBarItem(
                icon: Icon(Icons.chat_bubble),
                label: "Chat",
                backgroundColor: Colors.blue),
            BottomNavigationBarItem(
                icon: Icon(Icons.people),
                label: "Profile",
                backgroundColor: Colors.blue),
          ]),
    ));
  }
}
