import 'package:flutter/material.dart';
import 'package:lab5_ex1/plan_provider.dart';
import 'views/app.dart';
import 'plan_provider.dart';

void main() {
  var planProvider = PlanProvider(child: MyPlanApp());
  runApp(planProvider);
}
