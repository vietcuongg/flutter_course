import 'package:flutter/material.dart';

class TextStream {
  Stream<String> getMessages() async* {
    final List<String> messages = [
      'Goodmoring Viewer!',
      'Wellcome to my Channel',
      'like and subscride my channel',
      'Please Donate me'
    ];

    yield* Stream.periodic(Duration(seconds: 5), (int t) {
      int index = t % 7;
      return messages[index];
    });
  }
}
