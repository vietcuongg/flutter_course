import 'package:flutter/material.dart';
import 'package:lab6/data_provider.dart';
import './view/app.dart';

main() {
  runApp(DataProvider(child: MyApp()));
}
